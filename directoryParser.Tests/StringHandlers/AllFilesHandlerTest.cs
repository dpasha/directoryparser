﻿using directoryParser.Handlers;
using NUnit.Framework;

namespace directoryParser.Tests.StringHandlers
{
    [TestFixture]
    public class AllFilesHandlerTest
    {
        #region Mock data

        const string sourceFullPath = "/Users/user/Documents/project/testTask/File.cs";
        const string sourceDir = "/Users/user/Documents/project/";
        const string unexistedSourceDir = "/User/Peet/";
        const string sourceDirEmpty = "";
        const string filePathWitoutDir = "testTask/File.cs";

        #endregion

        [Test]
        public void ShouldRemovePartWithDirPath()
        {
            AllFilesHandler allFilesHandler = new AllFilesHandler();

            string result = allFilesHandler.HandleString(sourceFullPath, sourceDir);

            Assert.AreEqual(filePathWitoutDir, result);
        }

        [Test]
        public void ShouldNotRemovePartWithDirPath()
        {
            AllFilesHandler allFilesHandler = new AllFilesHandler();

            string result = allFilesHandler.HandleString(sourceFullPath, unexistedSourceDir);
            string result2 = allFilesHandler.HandleString(sourceFullPath, sourceDirEmpty);

            Assert.AreEqual(sourceFullPath, result);
            Assert.AreEqual(sourceFullPath, result2);
        }
    }
}