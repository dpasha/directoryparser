﻿using directoryParser.Handlers;
using NUnit.Framework;

namespace directoryParser.Tests.StringHandlers
{
    [TestFixture()]
    public class ReverseStringHandlerTest
    {
        #region Mock and expected data

        const string sourceFullPath = "project/testTask/File.txt";
        const string sourceDir = "project/";
        const string reversedStringWitoutDir = "txt.eliF/ksaTtset";

        #endregion

        [Test]
        public void ShouldReverseString()
        {
            ReverseStringHandler reverseStringHandler = new ReverseStringHandler();

            string result = reverseStringHandler.HandleString(sourceFullPath, sourceDir);

            Assert.AreEqual(reversedStringWitoutDir, result);
        }

        [Test]
        public void ShouldNotReverseString()
        {
            ReverseStringHandler reverseStringHandler = new ReverseStringHandler();

            string result = reverseStringHandler.HandleString(string.Empty, sourceDir);

            Assert.AreEqual(string.Empty, result);
        }
    }
}