﻿using directoryParser.Handlers;
using NUnit.Framework;

namespace directoryParser.Tests.StringHandlers
{
    [TestFixture()]
    public class ReversePathHandlerTest
    {
        #region Mock and expected data

        const string sourceFullPath = "project/testTask/File.txt";
        const string sourceDir = "project/";
        const string reversedPathWitoutDir = "File.txt/testTask";

        const string sourceLocalPath = "File.txt";

        #endregion

        [Test]
        public void ShouldReversePath() 
        {
            ReversePathHandler reversePathHandler = new ReversePathHandler();

            string result = reversePathHandler.HandleString(sourceFullPath, sourceDir);

            Assert.AreEqual(reversedPathWitoutDir, result);
        }

        [Test]
        public void ShouldNotReversePath()
        {
            ReversePathHandler reversePathHandler = new ReversePathHandler();

            string result = reversePathHandler.HandleString(sourceLocalPath, sourceDir);

            Assert.AreEqual(sourceLocalPath, result);
        }
    }
}