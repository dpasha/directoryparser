﻿using directoryParser.Handlers;
using NUnit.Framework;

namespace directoryParser.Tests.StringHandlers
{
    [TestFixture()]
    public class CsFilesHandlerTest
    {
        #region Mock and expected data

        const string sourceFullPath = "/Users/user/Documents/project/testTask/File.txt";
        const string sourceFullPathCs = "/Users/user/Documents/project/testTask/File.cs";
        const string sourceDir = "/Users/user/Documents/project/";
        const string unexistedSourceDir = "/User/Peet/";
        const string sourceDirEmpty = "";
        const string filePathWitoutDir = "testTask/File.cs/";
        const string expectedFullPathCs = "/Users/user/Documents/project/testTask/File.cs/";

        #endregion

        [Test]
        public void ShouldRemovePartWithDirPath()
        {
            CsFilesHandler csFilesHandler = new CsFilesHandler();

            string result = csFilesHandler.HandleString(sourceFullPathCs, sourceDir);

            Assert.AreEqual(filePathWitoutDir, result);
        }

        [Test]
        public void ShouldNotRemovePartWithDirPath()
        {
            CsFilesHandler csFilesHandler = new CsFilesHandler();

            string result = csFilesHandler.HandleString(sourceFullPath, sourceDir);
            string result2 = csFilesHandler.HandleString(sourceFullPathCs, unexistedSourceDir);
            string result3 = csFilesHandler.HandleString(sourceFullPathCs, sourceDirEmpty);

            Assert.AreEqual(string.Empty, result);
            Assert.AreEqual(expectedFullPathCs, result2);
            Assert.AreEqual(expectedFullPathCs, result3);
        }
    }
}