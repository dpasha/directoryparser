﻿using directoryParser.Common;
using directoryParser.Interfaces;
using directoryParser.Models;
using directoryParser.Parsers;
using NUnit.Framework;

namespace directoryParser.Tests.Parsers
{
    [TestFixture]
    public class NDeskArgsParserTest
    {
        #region Mock and expected data

        string[] fullTestArgs = { "-p", "/Users/pavel/Documents/test project/testTask/", "-a", "cs" };
        string[] testArgsWithoutRoot = { "-a", "cs" };
        string[] testArgsWithoutAction = { "-p", "/Users/pavel/Documents/test project/testTask/" };

        CommandArgs fullCommandArgs = new CommandArgs()
        {
            RootDirectoryPath = "/Users/pavel/Documents/test project/testTask/",
            ResultFilePath = string.Empty,
            Action = Actions.cs
        };

        #endregion

        [Test]
        public void ShouldParseArgsToModel()
        {
            NDeskArgsParser nDeskArgsParser = new NDeskArgsParser(logger: null);

            ICommandArgs result = nDeskArgsParser.ParseArgs(fullTestArgs);

            Assert.AreEqual(fullCommandArgs, result);
        }

        [Test]
        public void ShouldReturnNull()
        {
            NDeskArgsParser nDeskArgsParser = new NDeskArgsParser(logger: null);
            NDeskArgsParser nDeskArgsParser2 = new NDeskArgsParser(logger: null);

            var result = nDeskArgsParser.ParseArgs(testArgsWithoutRoot);
            var result2 = nDeskArgsParser2.ParseArgs(testArgsWithoutAction);

            //var argumentNullExceptionWithoutRoot = Assert.Throws<ArgumentNullException>(() => nDeskArgsParser.ParseArgs(testArgsWithoutRoot));
            //var argumentNullExceptionWithoutAction = Assert.Throws<ArgumentNullException>(() => nDeskArgsParser.ParseArgs(testArgsWithoutAction));

            //Assert.That(argumentNullExceptionWithoutRoot.Message, Is.EqualTo(Constants.ROOT_DIR_ABSENT));
            //Assert.That(argumentNullExceptionWithoutAction.Message, Is.EqualTo(Constants.ACTION_ABSENT));

            Assert.True(result == null);
            Assert.True(result2 == null);
        }
    }
}
