﻿using System;
using directoryParser.Common;

namespace directoryParser.Extensions
{
    public static class ExceptionExtension
    {
        public static string GetDetails(this Exception ex)
        {
            string details = string.Empty;

            while (ex != null)
            {
                details = string.Format(Constants.EXCEPTION_OUTPUT_TEMPLATE, ex.GetType(), ex.Message, ex.StackTrace);
                ex = ex.InnerException;
            }

            return details;
        }
    }
}
