﻿using System;
namespace directoryParser.Extensions
{
    public static class StringExtension
    {
        public static string Reverse(this string str)
        {
            char[] charArray = str.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
