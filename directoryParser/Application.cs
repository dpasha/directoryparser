﻿using directoryParser.Common;
using directoryParser.Interfaces;
using directoryParser.Services;

namespace directoryParser
{
    public class Application : IApplication
    {
        private ICommandArgsParser m_commandArgsParser;
        private IDirectoryService m_directoryService;
        private IStringHandlerService m_stringHandlerService;
        private IOutputService m_outputService;
        private ILogger m_logger;

        public Application(ICommandArgsParser commandArgsParser, 
                           IDirectoryService directoryService, 
                           IStringHandlerService stringHandlerService,
                           IOutputService outputService,
                           ILogger logger)
        {
            m_commandArgsParser = commandArgsParser;
            m_directoryService = directoryService;
            m_stringHandlerService = stringHandlerService;
            m_outputService = outputService;
            m_logger = logger;
        }

        public void Run(string[] args)
        {
            var commandArgsModel = m_commandArgsParser.ParseArgs(args);

            if(commandArgsModel == null)
            {
                Log(Constants.ERROR_DURING_PARSING_ARGS);
                return;
            }

            var filesList = m_directoryService.ParseDirectory(commandArgsModel);

            if(filesList.Count == 0)
            {
                Log(Constants.FILES_NOT_FOUND);
                return;
            }

            string resultText = m_stringHandlerService.GetModifiedStrings(filesList, commandArgsModel);

            if(!string.IsNullOrEmpty(resultText))
            {
                m_outputService.WriteResult(commandArgsModel.ResultFilePath, resultText);
            }
        }

        private void Log(string message) => m_logger?.Log(message);
    }
}