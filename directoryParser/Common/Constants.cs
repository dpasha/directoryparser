﻿
namespace directoryParser.Common
{
    public static class Constants
    {
        // args
        public static readonly string ROOT_DIR_ABSENT = "Root directory was not specified!";
        public static readonly string ACTION_ABSENT = "Action was not specified!";

        // common
        public static readonly string RESULT_FILE_NAME = "Results.txt";
        public static readonly string EXCEPTION_OUTPUT_TEMPLATE = "Type: {0}\nMessage: {1}\nStackTrace: {2}";
        public static readonly string UNKNOWN_PARAMETER_TEMPLATE = "Unknown parameter: {0}";
        public static readonly string NOT_A_MEMBER_OF_ACTIONS_TEMPLATE = "{0} is not a member of the Actions enumeration.";
        public static readonly string OPTIONS_HEADER = "Options:";

        // log's messages
        public static readonly string PARSING_PROCESS = "App is parsing directories...";
        public static readonly string ESC_TO_CANCEL = "Press Esc to cancel";
        public static readonly string FILES_FOUND_TEMPLATE = " Found files: {0} in {1} milliseconds.";
        public static readonly string FILE_WAS_UPDATED_TEMPLATE = "File {0} was updated successfully.";
        public static readonly string ERROR_DURING_PARSING_ARGS = "Error was occured during parsing command line args. App will be closed.";
        public static readonly string FILES_NOT_FOUND = "There are no files to handle...";
    }
}
