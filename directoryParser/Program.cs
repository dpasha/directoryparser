﻿using Autofac;

namespace directoryParser
{
    sealed class MainClass
    {
        public static void Main(string[] args)
        {
            var container = ContainerConfig.Confugure();

            using (var scope = container.BeginLifetimeScope())
            {
                var app = scope.Resolve<IApplication>();
                app.Run(args);
            }
        }
    }
}