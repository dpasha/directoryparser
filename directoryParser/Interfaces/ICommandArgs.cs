﻿using directoryParser.Common;

namespace directoryParser.Interfaces
{
    public interface ICommandArgs
    {
        string RootDirectoryPath { get; set; }
        Actions Action { get; set; }
        string ResultFilePath { get; set; }
    }
}