﻿namespace directoryParser
{
    public interface IApplication
    {
        void Run(string[] args);
    }
}