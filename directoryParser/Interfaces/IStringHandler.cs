﻿namespace directoryParser.Interfaces
{
    public interface IStringHandler
    {
        string HandleString(string filePath, string directoryPath);
    }
}
