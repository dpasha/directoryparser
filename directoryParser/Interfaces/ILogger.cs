﻿
namespace directoryParser.Interfaces
{
    public interface ILogger
    {
        void Log(string message);
    }
}
