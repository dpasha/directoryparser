﻿namespace directoryParser.Interfaces
{
    public interface IOutputService
    {
        // perhaps destination could be not always a string
        void WriteResult(object destination, string content);
    }
}
