﻿using System.Collections.Generic;
using directoryParser.Interfaces;

namespace directoryParser.Services
{
    public interface IDirectoryService
    {
        IList<string> ParseDirectory(ICommandArgs consoleArgs);
    }
}