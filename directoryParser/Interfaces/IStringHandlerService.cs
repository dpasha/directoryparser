﻿using System.Collections.Generic;
using directoryParser.Interfaces;

namespace directoryParser.Services
{
    public interface IStringHandlerService
    {
        string GetModifiedStrings(IList<string> filesList, ICommandArgs commandArgs);
    }
}