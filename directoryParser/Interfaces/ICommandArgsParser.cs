﻿using directoryParser.Models;

namespace directoryParser.Interfaces
{
    public interface ICommandArgsParser
    {
        ICommandArgs ParseArgs(string[] args);
    }
}
