﻿using System;
using System.Collections.Generic;
using System.Text;
using directoryParser.Common;
using directoryParser.Extensions;
using directoryParser.Handlers;
using directoryParser.Interfaces;

namespace directoryParser.Services
{
    public class StringHandlerService : IStringHandlerService
    {
        #region Ctor

        public StringHandlerService(ILogger logger)
        {
            m_logger = logger;
            m_stringBuilder = new StringBuilder();
        }

        #endregion

        #region Public Logic

        public string GetModifiedStrings(IList<string> filesList, ICommandArgs commandArgs)
        {
            try
            {
                m_stringHandler = GetAppropriateStringHandler(commandArgs.Action);

                foreach (var file in filesList)
                {
                    string resultString = GetModifiedString(file, commandArgs.RootDirectoryPath);
                    
                    if(!string.IsNullOrEmpty(resultString))
                    {
                        m_stringBuilder.Append(resultString + Environment.NewLine);
                    }
                }

                return m_stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                Log(ex.GetDetails());
                return string.Empty;
            }
        }

        #endregion

        #region Private Logic 

        private IStringHandler GetAppropriateStringHandler(Actions action)
        {
            //it could be done via factory pattern but I realized it should be enough for this task
            switch (action)
            {
                case Actions.all:
                    return new AllFilesHandler();
                case Actions.cs:
                    return new CsFilesHandler();
                case Actions.reversed1:
                    return new ReversePathHandler();
                case Actions.reversed2:
                    return new ReverseStringHandler();
                default:
                    throw new Exception(string.Format(Constants.NOT_A_MEMBER_OF_ACTIONS_TEMPLATE, action));
            }
        }

        private string GetModifiedString(string fileName, string dirPath) => m_stringHandler.HandleString(fileName, dirPath);

        private void Log(string message) => m_logger?.Log(message);

        #endregion

        #region Private Fields

        private IStringHandler m_stringHandler;
        private StringBuilder m_stringBuilder;
        private ILogger m_logger;

        #endregion
    }
}