﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using directoryParser.Common;
using directoryParser.Extensions;
using directoryParser.Interfaces;

namespace directoryParser.Services
{
    public class DirectoryService : IDirectoryService
    {
        #region Ctor

        public DirectoryService(ILogger logger)
        {
            m_logger = logger;
            m_filesList = new List<string>();
            m_cts = new CancellationTokenSource();
            m_ct = m_cts.Token;
            m_tasks = new ConcurrentBag<Task>();
        }

        #endregion

        #region Public Logic 

        public IList<string> ParseDirectory(ICommandArgs consoleArgs)
        {
            Log(Constants.PARSING_PROCESS);
            Log(Constants.ESC_TO_CANCEL);
            
            Stopwatch watch = new Stopwatch();
            watch.Start();
            CollectFolders(consoleArgs);
            watch.Stop();
            
            Log(string.Format(Constants.FILES_FOUND_TEMPLATE, m_foundFiles, watch.ElapsedMilliseconds));
            
            return m_filesList;
        }

        #endregion

        #region Private Logic

        private void CollectFolders(ICommandArgs commandArgs)
        {
            m_tasks.Add(Task.Run(() => {
                CrawlFolder(commandArgs.RootDirectoryPath, commandArgs.RootDirectoryPath);
            }, m_ct));

            Thread thread = new Thread(() =>
            {
                if (Console.ReadKey().Key == ConsoleKey.Escape)
                {
                    m_cts.Cancel();
                }
            });

            thread.Start();

            while (m_tasks.TryTake(out Task taskToWaitFor))
            {
                taskToWaitFor.Wait();
            }

            thread.Abort();
        }

        private void CrawlFolder(string path, string dirPath)
        {
            if (m_ct.IsCancellationRequested)
                return;
            
            try
            {
                foreach (var fileName in Directory.GetFiles(path))
                {
                    m_filesList.Add(fileName);
                    Interlocked.Increment(ref m_foundFiles);
                }
                
                foreach (var localPath in Directory.GetDirectories(path))
                {
                    m_tasks.Add(Task.Run(() => CrawlFolder(localPath, dirPath)));
                }
            }
            catch (Exception ex)
            {
                Log(ex.GetDetails());
            }
        }

        private void Log(string message) => m_logger?.Log(message);

        #endregion

        #region Private Fields
        
        private ILogger m_logger;
        private IList<string> m_filesList;
        private int m_foundFiles;
        private ConcurrentBag<Task> m_tasks;
        private CancellationTokenSource m_cts;
        private CancellationToken m_ct;

        #endregion
    }
}