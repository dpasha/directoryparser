﻿using System;
using System.IO;
using directoryParser.Common;
using directoryParser.Interfaces;

namespace directoryParser.Services
{
    public class FileOutputService : IOutputService
    {
        #region Ctor

        public FileOutputService(ILogger logger)
        {
            m_logger = logger;
        }

        #endregion

        #region Public Logic

        public void WriteResult(object destination, string content)
        {
            string resultFilePath = Convert.ToString(destination);
            resultFilePath = string.IsNullOrEmpty(Convert.ToString(destination)) ? Constants.RESULT_FILE_NAME : resultFilePath;
            
            try
            {
                File.WriteAllText(resultFilePath, content);
                Log(string.Format(Constants.FILE_WAS_UPDATED_TEMPLATE, resultFilePath));
            }
            catch (Exception ex)
            {
                Log(string.Format(Constants.EXCEPTION_OUTPUT_TEMPLATE, ex.Message));
            }
        }

        #endregion

        #region Private Logic

        private void Log(string message) => m_logger?.Log(message);

        #endregion

        #region Private Fields

        private ILogger m_logger;

        #endregion
    }
}
