﻿using directoryParser.Interfaces;

namespace directoryParser.Handlers
{
    public class AllFilesHandler : IStringHandler
    {
        public virtual string HandleString(string filePath, string directoryPath)
        {
            return string.IsNullOrEmpty(directoryPath) || string.IsNullOrEmpty(filePath) 
                            ? filePath 
                            : filePath.Replace(directoryPath, string.Empty);
        }
    }
}
