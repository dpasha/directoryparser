﻿using System;
using System.IO;

namespace directoryParser.Handlers
{
    public class ReversePathHandler : AllFilesHandler
    {
        public override string HandleString(string filePath, string directoryPath)
        {
            string localPath = base.HandleString(filePath, directoryPath);

            if (string.IsNullOrEmpty(localPath))
                return string.Empty;

            string[] pathItems = localPath.Split(Path.DirectorySeparatorChar);
            Array.Reverse(pathItems);

            return string.Join("/", pathItems);
        }
    }
}
