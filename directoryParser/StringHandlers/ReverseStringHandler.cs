﻿using directoryParser.Extensions;

namespace directoryParser.Handlers
{
    public class ReverseStringHandler : AllFilesHandler
    {
        public override string HandleString(string filePath, string directoryPath)
        {
            string localPath = base.HandleString(filePath, directoryPath);

            if (string.IsNullOrEmpty(localPath))
                return string.Empty;

            return localPath.Reverse();
        }
    }
}
