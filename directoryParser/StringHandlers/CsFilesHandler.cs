﻿using System.IO;

namespace directoryParser.Handlers
{
    public class CsFilesHandler : AllFilesHandler
    {
        public override string HandleString(string filePath, string directoryPath)
        {
            string localPath = base.HandleString(filePath, directoryPath);
            string extension = Path.GetExtension(localPath);

            if (string.IsNullOrEmpty(extension))
                return string.Empty;

            return Path.GetExtension(localPath).Equals(".cs")
                    ? localPath += '/'
                    : string.Empty;
        }
    }
}
