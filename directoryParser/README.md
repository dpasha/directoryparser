# directoryParser #
this app parses directories, finds all files and handles it in specified manner

## Getting Started ##

## Options: ##
- -p, --path=VALUE           root directory path
- -a, --action=VALUE         name of action
- -r, --res=VALUE            the path to result's file (Result.txt by default)
- -h, --help                 show help's desctiption
  
## Available actions: ##
- all - get all files, remove dirPath and save it to file
- cs - get .cs files only, remove dirPath and save it to file
- reversed1 - get all files, remove dirPath, reverse path to file and save it to file
- reversed1 - get all files, remove dirPath, reverse string and save it to file
  
## Examples:##
  
- "directoryParser.exe" -p /Documents -r newFile.txt -a reversed2 -h

- "directoryParser.exe" -p /Documents -a cs
