﻿using System;
using System.Collections.Generic;
using directoryParser.Common;
using directoryParser.Extensions;
using directoryParser.Interfaces;
using directoryParser.Models;
using NDesk.Options;

namespace directoryParser.Parsers
{
    public class NDeskArgsParser : ICommandArgsParser
    {
        #region Ctors

        public NDeskArgsParser(ILogger logger)
        {
            m_logger = logger;

            m_OptionSet = new OptionSet() {
                { "p|path=", "root directory path",         v =>  m_RootDirecoryPath = v ?? string.Empty },
                { "a|action=", "name of action",            v => m_ActionName = v ?? string.Empty },
                { "r|res=",  "the path to result's file",   v => m_ResultFilePath = v ?? string.Empty },
                { "h|help", "show help's desctiption",      v => m_ShowHelp = v != null }
            };
        }

        #endregion

        #region Public Logic 

        public ICommandArgs ParseArgs(string[] args)
        {
            List<string> extra;
            try
            {
                extra = m_OptionSet.Parse(args);

                if (string.IsNullOrEmpty(m_RootDirecoryPath))
                {
                    throw new ArgumentNullException(Constants.ROOT_DIR_ABSENT);
                }
                
                if (string.IsNullOrEmpty(m_ActionName))
                {
                    throw new ArgumentNullException(Constants.ACTION_ABSENT);
                }

                foreach (var item in extra)
                {
                    Log(string.Format(Constants.UNKNOWN_PARAMETER_TEMPLATE, item));
                }

                if (!Enum.TryParse(m_ActionName.ToLower(), out Actions action))
                {
                    throw new Exception(string.Format(Constants.NOT_A_MEMBER_OF_ACTIONS_TEMPLATE, m_ActionName));
                }

                if (m_ShowHelp)
                {
                    ShowHelp();
                }

                return new CommandArgs()
                {
                    RootDirectoryPath = m_RootDirecoryPath,
                    ResultFilePath = m_ResultFilePath,
                    Action = action
                };
            }
            catch (Exception e)
            {
                Log(e.GetDetails());
                ShowHelp();
                return null;
            }
        }

        #endregion

        #region Private Logic 

        private void ShowHelp()
        {
            Console.WriteLine(Constants.OPTIONS_HEADER);
            m_OptionSet.WriteOptionDescriptions(Console.Out);
        }

        private void Log(string message) => m_logger?.Log(message);

        #endregion

        #region Private Fields

        private OptionSet m_OptionSet = null;
        private bool m_ShowHelp = false;
        private string m_RootDirecoryPath = string.Empty;
        private string m_ResultFilePath = string.Empty;
        private string m_ActionName = string.Empty;
        private ILogger m_logger;

        #endregion
    }
}