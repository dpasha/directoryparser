﻿using Autofac;
using directoryParser.Helpers;
using directoryParser.Interfaces;
using directoryParser.Parsers;
using directoryParser.Services;

namespace directoryParser
{
    public static class ContainerConfig
    {
        public static IContainer Confugure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Application>().As<IApplication>();
            builder.RegisterType<DirectoryService>().As<IDirectoryService>();
            builder.RegisterType<NDeskArgsParser>().As<ICommandArgsParser>();
            builder.RegisterType<StringHandlerService>().As<IStringHandlerService>();
            builder.RegisterType<FileOutputService>().As<IOutputService>();
            builder.RegisterType<ConsoleLogger>().As<ILogger>();

            return builder.Build();
        }
    }
}
