﻿using System;
using directoryParser.Interfaces;

namespace directoryParser.Helpers
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
