﻿using directoryParser.Common;
using directoryParser.Interfaces;

namespace directoryParser.Models
{
    public class CommandArgs : ICommandArgs
    {
        public string RootDirectoryPath { get; set; }
        public Actions Action { get; set; }
        public string ResultFilePath { get; set; }

        public override bool Equals(object obj)
        {
            return obj is CommandArgs commandArgs
                    && commandArgs.Action == Action
                    && commandArgs.ResultFilePath == ResultFilePath
                    && commandArgs.RootDirectoryPath == RootDirectoryPath;
        }
    }
}
